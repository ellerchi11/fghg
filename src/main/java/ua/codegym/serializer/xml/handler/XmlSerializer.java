package ua.codegym.serializer.xml.handler;

import ua.codegym.serializer.Serializer;
import ua.codegym.serializer.shape.Group;
import ua.codegym.serializer.shape.Shape;
import ua.codegym.serializer.shape.Square;

import java.io.OutputStream;
import java.util.HashMap;
import java.util.Map;

public class XmlSerializer implements Serializer {

    public static final XmlSerializer XML = new XmlSerializer();

    private Map<String, Serializer> serializerMap = new HashMap<>();

    private XmlSerializer() {
        serializerMap.put(Square.class.getCanonicalName(), new SquareXmlHandler());
      serializerMap.put(Group.class.getCanonicalName(), new  GroupXmlHandler());
    }

        public Serializer getSerializer(Shape shape){
       return  this.serializerMap.get(shape.getClass().getCanonicalName());
        }
    public void serialize(Shape shape, OutputStream os) {
             this.serializerMap.get(shape.getClass().getCanonicalName()).serialize(shape, os);
    }
}
