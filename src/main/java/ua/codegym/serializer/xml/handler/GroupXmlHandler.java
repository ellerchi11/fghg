package ua.codegym.serializer.xml.handler;

import ua.codegym.serializer.Serializer;
import ua.codegym.serializer.shape.Group;
import ua.codegym.serializer.shape.Shape;
import ua.codegym.serializer.shape.Square;

import java.io.IOException;
import java.io.OutputStream;

public class GroupXmlHandler extends AbstractSerializer implements Serializer {

  @Override
  public void serialize(Shape shape, OutputStream os) {
  if (!(shape instanceof Group)) {
    throw new RuntimeException("Incorrect shape" + shape) ;
  }

  Group group= (Group) shape;



 write("<group>", os);
 group.getShapes().forEach((eShape)-> XmlSerializer.XML.getSerializer(eShape).serialize(eShape, os));
write("</group>",os);

  }
}