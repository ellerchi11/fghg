1. (Must have!) [Refactoring by Robert C. Marting](https://www.youtube.com/watch?v=y4_SJzNJnXU) [20 min]
2. (Must have!) [Three laws of TDD](https://www.youtube.com/watch?v=qkblc5WRn-U)
3. [Summary of Clean Code](https://gist.github.com/wojteklu/73c6914cc446146b8b533c0988cf8d29) [10 min]
4. [Polymorphism](https://docs.oracle.com/javase/tutorial/java/IandI/polymorphism.html) [15 min]
5. [Composite Design Pattern](https://dzone.com/articles/composite-design-pattern-java-0) [20 min]


---

6. [Clean Code](https://www.amazon.com/Clean-Code-Handbook-Software-Craftsmanship/dp/0132350882/ref=sr_1_1?ie=UTF8&qid=1505244843&sr=8-1&keywords=clean+code)
